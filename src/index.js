class MyArray {
  constructor(...args) {
    Object.defineProperty(this, 'length', {
      writable: true
    });

    if (args.length === 0) {
      this.length = 0;
    }

    if (
      (args.length === 1 && typeof args[0] === 'number') ||
      (args.length === 1 && args[0] < 0)
    ) {
      if (args[0] > 0 && Number.isInteger(args[0])) {
        this.length = args[0];
      } else {
        throw Error('Invalid array length');
      }
    } else {
      for (let i = 0; i < args.length; i += 1) {
        this[i] = args[i];
      }
      this.length = args.length;
    }
  }

  push(...args) {
    for (let i = 0; i < args.length; i += 1) {
      this[i + this.length] = args[i];
    }
    this.length = args.length + this.length;

    return this.length;
  }

  pop() {
    const el = this[this.length - 1];
    delete this[this.length - 1];

    this.length = this.length - 1;
    return el;
  }

  unshift(arg, ...args) {
    const totalArgs = new MyArray(arg, ...args);

    if (!arg) {
      return this.length;
    }

    const del = totalArgs.length;
    const thisLength = this.length;
    const fullLength = thisLength + del;

    for (let i = fullLength - 1; i >= 0; i -= 1) {
      if (i >= del) {
        this[i] = this[i - del];
      } else {
        this[i] = totalArgs[i];
      }
    }

    this.length = fullLength;
    return this.length;
  }

  shift() {
    if (this.length === 0) {
      return undefined;
    }

    const el = this[0];

    for (let i = 1; i < this.length; i += 1) {
      this[i - 1] = this[i];
    }

    delete this[this.length - 1];
    this.length = this.length - 1;

    return el;
  }

  forEach(callback, ...thisArgs) {
    const thisArg = thisArgs.length > 0 ? thisArgs[0] : 'undefined';
    const newLength = this.length;

    for (let i = 0; i < newLength; i++) {
      if (i in this) {
        callback.call(thisArg, this[i], i, this);
      }
    }
  }

  map(callback, ...thisArgs) {
    const newArr = new MyArray();
    const thisArg = thisArgs.length > 0 ? thisArgs[0] : undefined;

    for (let i = 0; i < this.length + 1; i += 1) {
      if (i in this) {
        newArr[i] = callback.call(thisArg, this[i], i, this);
        newArr.length += 1;
      }
    }

    return newArr;
  }

  filter(callback, ...thisArgs) {
    const newArr = new MyArray();
    const thisArg = thisArgs.length > 0 ? thisArgs[0] : undefined;
    const thisLength = this.length;
    let key = 0;

    for (let i = 0; i < thisLength; i += 1) {
      if (i in this) {
        if (callback.call(thisArg, this[i], i, this)) {
          newArr[key] = this[i];
          newArr.length += 1;
          key += 1;
        }
      }
    }
    return newArr;
  }

  reduce(callback, ...args) {
    if (args.length > 0 && this.length === 0) {
      return args[0];
    }

    let i = 0;
    let accum = args[0];
    let index = 0;

    while (index < this.length && !(index in this)) {
      index += 1;
    }

    if (index >= this.length) {
      throw new TypeError('Reduce of empty array with no initial value');
    }

    if (args[0]) {
      i = 0;
    } else {
      i = 1;
      accum = this[0];
    }

    for (i; i < this.length; i += 1) {
      accum = callback(accum, this[i], i, this);
    }
    return accum;
  }

  toString() {
    let newStr = '';

    for (let i = 0; i < this.length; i += 1) {
      if (this[i] === undefined || this[i] === null) {
        this[i] = '';
      }
      newStr += String(this[i]) + (i === this.length - 1 ? '' : ',');
    }

    return newStr;
  }

  sort(compare) {
    function compareDefault(a, b) {
      const aStr = String(a);
      const bStr = String(b);

      if (aStr > bStr) {
        return 1;
      }

      if (aStr === bStr) {
        return 0;
      }

      if (aStr < bStr) {
        return -1;
      }
    }

    const comparator = compare || compareDefault;
    const count = this.length - 1;

    for (let i = 0; i < count; i += 1) {
      let swaped = false;

      for (let j = 0; j < count - i; j += 1) {
        if (this[j + 1] === undefined) {
          swaped = true;
        } else if (
          this[j] === undefined ||
          comparator(this[j], this[j + 1]) > 0
        ) {
          const max = this[j];
          this[j] = this[j + 1];
          this[j + 1] = max;
          swaped = true;
        }
      }

      if (!swaped) {
        break;
      }
    }
    return this;
  }

  static from(arrayLike, ...args) {
    const newArr = new MyArray();
    const mapFn = args[0];
    const thisArg = args[1];

    if (typeof arrayLike === 'number' && arrayLike.length === 1) {
      return newArr;
    }

    for (let i = 0; i < arrayLike.length; i += 1) {
      // if ({}.hasOwnProperty.call(arrayLike, i)) {
      if (mapFn) {
        newArr[i] = mapFn.call(thisArg, arrayLike[i], i, arrayLike);
      } else {
        newArr[i] = arrayLike[i];
      }
      newArr.length += 1;
      // }
    }
    return newArr;
  }

  [Symbol.iterator]() {
    let index = -1;
    const that = this;

    return {
      next() {
        if (index < that.length - 1) {
          index += 1;
          return {
            done: false,
            value: that[index]
          };
        } else {
          return {
            done: true
          };
        }
      }
    };
  }
}

export default MyArray;
