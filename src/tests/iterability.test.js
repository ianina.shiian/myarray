import MyArray from '../index';

describe('tests for instance iterability', () => {
  test('instance has not Own Property Symbol.iterator', () => {
    const arr = new MyArray();
    expect(
      Object.prototype.hasOwnProperty.call(arr, Symbol.iterator)
    ).toBeFalsy();
  });

  test('should have Symbol.iterator method', () => {
    const arr = new MyArray();
    expect(arr[Symbol.iterator]).toBeInstanceOf(Function);
  });

  test('Symbol.iterator method should work correctly', () => {
    const arr = new MyArray(1, 2, 3, 4);
    expect([...arr]).toEqual([1, 2, 3, 4]);
  });
});
